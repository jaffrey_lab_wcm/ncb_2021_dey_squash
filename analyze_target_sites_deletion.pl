#!/usr/bin/perl
use strict;
use warnings;
use Algorithm::NeedlemanWunsch;

my $fastq_file = $ARGV[0];
my $template_file = $ARGV[1];
my $regexp_file = $ARGV[2];

open (FASTQ, "$fastq_file") || die "Cant open file: $!";
open (TEMPLATE, "$template_file") || die "Cant open file: $!";
open (REGEXP, "$regexp_file") || die "Cant open file: $!";

my @templates = <TEMPLATE>;
chomp (@templates);

my $regexp = <REGEXP>;
chomp ($regexp);

#my @templates = ('tagtgccaactg', 'cgtagcgtatac', 'tagtgccaactg', 'cgtagcgtatac');
my (@a, @b);
#my @b;

my @alignment;

while (my $seq = <FASTQ>){
	chomp ($seq);
	my $line_num = $.;
	next if ( $line_num == 1 or ( $line_num -2 ) % 4 != 0 ) ;
	my @targets;

	#if (@targets = $seq =~ m/\w{6}ATGC(\w{1,12})ATGC(\w{1,12})ATGC(\w{1,12})ATGC(\w{1,12})GATCG/ ){
	if (@targets = $seq =~ m/$regexp/ ){
		#my @targets = ($1,$2,$3,$4);

		if ($#targets != $#templates){
			print "ERROR: Number of targets and number of matches from regular expression do not match\!\n";
			last;
		}

		my @output;
		for my $i (0..$#targets){
			@a = split //, uc ( $targets[$i] );
			@b = split //, uc ( $templates[$i] ); 

			my $matcher = Algorithm::NeedlemanWunsch->new(\&score_sub);
            my $score = $matcher->align( \@a, \@b, {align => \&on_align, shift_a => \&on_shift_a, shift_b => \&on_shift_b} );
            @alignment = reverse (@alignment);

            my @edits = &analyze_alignments (\@alignment, $#templates); # @alignments is global

            #foreach my $temp_array_ref (@alignment) {
            #	print "@{ $temp_array_ref }", "\n";
            #}

            push @output, "target\_$i\: ".join (',', @edits);
            @alignment = ();
		}
		print "seq\.$line_num\t", $seq, "\t", join ("\t", @output), "\n";
	}
}

sub score_sub {
  if (!@_) { return -2; }            # gap penalty
  return ($_[0] eq $_[1]) ? 1 : -1;  # mismatch scores -1, match +1
} 

sub on_align  { push @alignment, [ $a[$_[0]], $b[$_[1]] ] };
sub on_shift_a { push @alignment, [ $a[$_[0]], '-' ] };
sub on_shift_b { push @alignment, [ '-', $b[$_[0]] ] };

#sub on_align  { print "align", " " , $a[$_[0]], ($a[$_[0]] eq $b[$_[1]] ) ? "-" : " ", $b[$_[1]], "\n"}; 
#sub on_shift_a {  print "gap  ", "" , $a[$_[0]], "\n" };
#sub on_shift_b { print "gap  ", "   " , $b[$_[0]], "\n"};


sub analyze_alignments{
	my ($alignment_ref, $length_template) = @_;
	my @alignment = @{ $alignment_ref };
	#my @mutation = @deletion = @insertion = ('' x $length_template);
	my @edits = ();

	my $temp_nuc_counter = 0;
	foreach my $line ( @alignment ){
		my ( $target_nuc, $template_nuc ) = @{ $line };
		$temp_nuc_counter++ if $template_nuc ne '-';

		#if ($target_nuc eq $template_nuc){ # if match
		#	push @edits, $target_nuc;
		#}
		if ( $target_nuc ne $template_nuc ){ # if not a match
			if ( $target_nuc eq '-' ){ # if deletion
				push @edits, 'D'.$temp_nuc_counter.$target_nuc;
			}
			elsif( $template_nuc eq '-' ){ # if insertion
				#if ($temp_nuc_counter >1 and $insertion [$temp_nuc_counter - 1] =~ m/\-/){
				if (exists $edits[-1] and $edits[-1] =~ m/I/){
					$edits[-1] .= $target_nuc;
				}
				else{
					push @edits, 'I'.$temp_nuc_counter.$target_nuc;
				}
			}
			else{ # if mismatch
				push @edits, '*'.$temp_nuc_counter.$target_nuc;
			}
		}
	}
	return (@edits);
}



#sub on_align  { print "align", " " , $a[$_[0]], ($a[$_[0]] eq $b[$_[1]] ) ? "-" : " ", $b[$_[1]], "\n"}; 
#sub on_shift_a {  print "gap  ", "" , $a[$_[0]], "\n" };
#sub on_shift_b { print "gap  ", "   " , $b[$_[0]], "\n"};

