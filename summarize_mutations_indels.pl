#!/usr/bin/perl
use strict;
use warnings;

my $indel_file = $ARGV[0];
my $template_file = $ARGV[1];

open (IN, "$indel_file") || die "Cant open file: $!";
open (TEMPLATE, "$template_file") || die "Cant open file: $!";

my @templates = <TEMPLATE>;
chomp (@templates);

my @template_sizes = map { length ($_) } @templates;
my %target_summary = ();

my $num_sequences = 0;
while (<IN>){
	chomp;
	$num_sequences++;

	my @split_line = split(/\t/, $_);

	for my $i ( 2..$#split_line ) {
		my ($target_num, $edit_list) = split (/\:\s/, $split_line[ $i ]);
		next if $edit_list eq '';
		#my ($target_num, $edit_list) = $split_line[ $i ] =~ m/\target_(\d): (.*)/;
		my @edits = split (/\,/, $edit_list);
		foreach (@edits){
			my ($type, $position, $nuc) = m/(.)(\d+)(.*)/;
			if (! exists $target_summary{ $target_num }{ $position }{ $type }{ $nuc } ){
				$target_summary{ $target_num }{ $position }{ $type }{ $nuc } = 0;
			} 
			$target_summary{ $target_num }{ $position }{ $type }{ $nuc }++;
		}
	}
}

print "\# Number of sequences analyzed\: ", $num_sequences, "\n";
print "\# Templates for target sequences\: @templates\n";

print "target\tposition\tmut_type\tnuc_change\tfreq\n";
foreach my $target_num ( sort keys %target_summary ){
	foreach my $position ( sort keys % { $target_summary { $target_num } } ){
		foreach my $type ( sort keys % { $target_summary { $target_num }{ $position } } ){
			foreach my $nuc ( sort keys % { $target_summary { $target_num }{ $position }{$type} } ){
				my $count = $target_summary { $target_num }{ $position }{ $type }{ $nuc };
				print join ("\t", ($target_num, $position, $type, $nuc, $count)), "\n";
			}
		}
	}
}
